import cv2
from personsegmentation import *
import cvui
import numpy as np
from tkinter import *
import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *


# Class for GUI
class WindowGUI(QWidget):
    def __init__(self):
        super().__init__()
        self.create_ui()

    def create_ui(self):
        # Initializing the GUI window to a certain size and initializing the
        self.setWindowTitle("MRO User's Choice")
        self.setGeometry(200, 200, 800, 300)
        self.users_choice()
        self.show()

    def users_choice(self):
        # Initializing the vertical and horizontal layouts
        hbox = QHBoxLayout()
        vbox = QVBoxLayout()

        # Defining the sub title
        self.label = QLabel('Draw It! - University of Malta AI Class 2018 - 2020', self)
        vbox.addWidget(self.label)

        # Defining the sub title
        choicebox = QGroupBox("Choose your preferred image")

        # Initializing the radio button for choosing the intersection image and adding it to the horizontal layout
        self.choice1 = QRadioButton("Intersection Image")
        hbox.addWidget(self.choice1)
        # Checking the value of the radio button
        self.choice1.toggled.connect(self.toggle_check)

        # Initializing the radio button for choosing the normal image and adding it to the horizontal layout
        self.choice2 = QRadioButton("Normal Image")
        hbox.addWidget(self.choice2)
        # Checking the value of the radio button
        self.choice2.toggled.connect(self.toggle_check)

        self.setLayout(vbox)

        # Initializing a slider
        self.slider = QSlider(Qt.Horizontal)
        self.slider.setFocusPolicy(Qt.StrongFocus)
        self.slider.setTickPosition(QSlider.TicksBothSides)
        # Slider has a minimum of 0 and a maximum of 100
        self.slider.setMinimum(0)
        self.slider.setMaximum(100)
        self.slider.setTickPosition(QSlider.TicksBothSides)
        self.slider.setTickInterval(10)
        self.slider.setSingleStep(1)
        # The initial value of the slider is set to 50
        self.slider.setValue(50)
        # Calling a method to change the image whenever the slider is released
        self.slider.sliderReleased.connect(self.toggle_check)

        choicebox.setLayout(hbox)
        # Adding the slider and the radio buttons to the layout
        vbox.addWidget(self.slider)
        vbox.addWidget(choicebox)

    def toggle_check(self):
        # Converting the value of the slider to a value between 0 and 1
        sigma = self.slider.value()/100
        if self.choice1.isChecked():
            # When intersection image is chosen

            # Generating an auto canny image for both the person segmented image and for the starndard one
            edged_img = auto_canny(gray_blurred_image(personSegmentedImage, kernalSize), sigma)
            normal_img = auto_canny(gray_blurred_image(image, kernalSize), sigma)

            # Taking the intersection of the above two images
            intersection_img = cv2.bitwise_and(edged_img, normal_img)

            print('Intersection image chosen and saved')

            # Displaying the image to the user
            cv2.imshow("Image", intersection_img)

            # Saving the image
            cv2.imwrite("GeneralSolutionImages//intersection//"+"image"+str(imageNumber)+"kernalSize"+str(kernalSize)+".jpg",intersection_img)
        elif self.choice2.isChecked():
            # When normal image is chosen

            # Generating an auto canny image of the standard image only
            normal_img = auto_canny(gray_blurred_image(image, kernalSize), sigma)

            print('Normal image chosen and saved')

            # Displaying the image to the user
            cv2.imshow("Image", normal_img)

            # Saving the image
            cv2.imwrite("GeneralSolutionImages//normal//" + "image" + str(imageNumber) + "kernalSize" + str(kernalSize) + ".jpg", normal_img)
        elif self.choice1.isChecked() and self.choice2.isChecked():
            # If both options are chosen
            print('Please choose one option')
        else:
            # If no options are chosen
            print('Please choose an option')


def auto_canny(image, sigma=.2):
    # Compute the median of the single channel pixel intensities
    v = np.median(image)

    # Apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)

    # Return the edged image
    return edged


def gray_blurred_image(image, kernalSize):
    # Converting the image to gray and applying a gaussian blur with the specified kernal size
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (kernalSize, kernalSize), 0)
    return blurred


if __name__ == '__main__':
    # Defining the kernal size and the image number to be used
    kernalSize = 3
    imageNumber = 5

    # Obtaining the normal image and person segmented image
    image = cv2.imread("original_images/"+str(imageNumber)+ ".jpg")
    personSegmentedImage = personsegmentation(str(imageNumber))

    # Initializing the gui window
    app = QApplication(sys.argv)
    window = WindowGUI()
    app.exit(app.exec())
